﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;

namespace socket_app
{
    public class Functions
    {
        // Declarable variables
        private static byte[] dataStream = new byte[1024]; // change the stream size later
        private static int port = Convert.ToInt16(ConfigurationManager.AppSettings.Get("port")); // take port from app.config
        private static string documentRoot = ConfigurationManager.AppSettings.Get("documentRoot"); // take doc root from app.config
        /* List to support multiple clients requests */
        private static List<Socket> clientSockets = new List<Socket>();
        /* Create socket */
        private static Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        /// <summary>
        /// Main logic function
        /// </summary>
        public static void Init()
        {
            try
            {
                Console.WriteLine("Starting the server..");
                Console.Title = "Server";
                socket.Bind(new IPEndPoint(IPAddress.Any, port));
                // Maximum pending connections, the other connections are refused
                socket.Listen(5);
                socket.BeginAccept(new AsyncCallback(AcceptCallback), null);
                Console.WriteLine("Server has started..");
            }
            catch (SocketException se)
            {
                Console.WriteLine("SocketException with the following details : {0}", se.Message);
            }
            catch (Exception exc)
            {
                Console.WriteLine("Uncaught exception with the following details: {0}", exc.Message);
            }
        }
        /// <summary>
        /// Recursive function which accepts the socket connection and begins work
        /// </summary>
        /// <param name="ar">IAsyncResult parameter which has data to work with</param>
        private static void AcceptCallback(IAsyncResult ar)
        {
            Socket s = socket.EndAccept(ar);
            clientSockets.Add(s);
            Console.WriteLine("Client has connected...");
            s.BeginReceive(dataStream, 0, dataStream.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), s);
            socket.BeginAccept(new AsyncCallback(AcceptCallback), null);
        }

        /// <summary>
        /// Function to receive callback function which handles the received data
        /// </summary>
        /// <param name="ar"></param>
        private static void ReceiveCallback(IAsyncResult ar)
        {
            Socket s = (Socket)ar.AsyncState;
            int receivedDataSize = 0;
            try
            {
                receivedDataSize = s.EndReceive(ar);
            }
            catch (SocketException se)
            {
                Console.WriteLine("An error has occured while receiving data: {0}", se.Message);
            }
            // nukopijuojam tai kas parejo i nauja streama, kuris nebus kosminio dydzio
            byte[] receivedRequestStream = new byte[receivedDataSize];
            Array.Copy(dataStream, receivedRequestStream, receivedDataSize);

            string msg = Encoding.ASCII.GetString(receivedRequestStream);
            string input = "";
            if (msg.Length > 0)
            {
                try
                {
                    input = msg.Substring(msg.IndexOf('/') + 1, msg.IndexOf(' ')).Split(' ')[0]; // this will not work for multiple words file name
                }
                catch (Exception exc)
                {
                    Console.WriteLine("Exception occured: {0}", exc.Message);
                }
            }

            Console.WriteLine("Request received: {0}", input);

            // Data received and is handled - continue to work
            string output = "";
            // Added supported formats for file to read, have tested though only txt html for a proof of concept
            string[] supportedFormats = { ".txt", ".html" };
            string response = "";
            if (!hasSpecialCharacters(input))
            {
                foreach (string extension in supportedFormats)
                {
                    if (File.Exists(documentRoot + "\\" + input + extension)) /* input = filename given, f */
                    {
                        output = File.ReadAllText(documentRoot + "\\" + input + extension);
                    }
                }
                if (!string.IsNullOrWhiteSpace(output))
                {
                    response = formatResponse("200", output);
                }
                else
                {
                    response = formatResponse("404", output);
                }
            }
            else
            {
                response = formatResponse("400", output);
            }

            Console.WriteLine("Sending back: {0}", output);
            // Send information back to the browser
            byte[] data = Encoding.ASCII.GetBytes(response);
            s.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(SendCallBack), s);
        }

        /// <summary>
        /// Function to return the output according to the status headers
        /// </summary>
        /// <param name="errorCode"></param>
        /// <param name="output"></param>
        /// <returns></returns>
        private static string formatResponse(string errorCode, string output)
        {
            switch (errorCode)
            {
                case "200":
                    return "HTTP/1.1 200 OK\r\n" + "Accept: */*\r\nHost: localhost:8080\r\nAccept-Encoding: gzip, deflate, br\r\nConnection: keep-alive\r\n\r\n" + "<html><head><title>OK</title></head><body><div>" + output + "</div></body></html>";
                case "400":
                    return "HTTP/1.1 400 BAD REQUEST\r\n" + "Accept: */*\r\nHost: localhost:8080\r\nAccept-Encoding: gzip, deflate, br\r\nConnection: keep-alive\r\n\r\n" + "<html><head><title>BAD REQUEST</title></head><body><div>You have used the symbol that is not allowed in request</div></body></html>";
                case "404":
                    return "HTTP/1.1 404 NOT FOUND\r\n" + "Accept: */*\r\nHost: localhost:8080\r\nAccept-Encoding: gzip, deflate, br\r\nConnection: keep-alive\r\n\r\n" + "<html><head><title>NOT FOUND</title></head><body><div>Your request was not found</div></body></html>";
                default:
                    Console.WriteLine("Unexpected error");
                    break;
            }
            // switchas grazina content type
            return "Unexpected error occured";
        }


        /// <summary>
        /// Function using Regular Expressions to check if the input is valid and does
        /// not contain special chars which are not allowed to be used in file names.
        /// </summary>
        /// <param name="input">String input to check</param>
        /// <returns></returns>
        private static bool hasSpecialCharacters(string input)
        {
            var regexItem = new Regex("^[a-zA-Z0-9]*$");
            return regexItem.IsMatch(input) ? false : true;
        }
        /// <summary>
        /// Function for sending response to the browser
        /// </summary>
        /// <param name="ar"></param>
        private static void SendCallBack(IAsyncResult ar)
        {
            try
            {
                Socket s = (Socket)ar.AsyncState;
                s.EndSend(ar);
                s.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception occured: {0}", e.Message);
            }
        }
    }
}

